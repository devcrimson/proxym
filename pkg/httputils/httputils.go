package httputils

import (
	"context"
	"net/http"
	url2 "net/url"
)

func Get(url string, ctx context.Context) (*http.Response, error) {
	req, err := http.NewRequestWithContext(ctx, "GET", url, nil)
	if err != nil {
		return nil, err
	}
	return http.DefaultClient.Do(req)
}

func GetProxied(url string, proxy string, ctx context.Context) (*http.Response, error) {
	u, err := url2.Parse(proxy)
	if err != nil {
		return nil, err
	}
	req, err := http.NewRequestWithContext(ctx, "GET", url, nil)
	if err != nil {
		return nil, err
	}
	client := http.Client{
		Transport: &http.Transport{
			Proxy: http.ProxyURL(u),
		},
	}
	return client.Do(req)
}
