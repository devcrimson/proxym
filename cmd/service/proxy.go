package main

import (
	"context"
	"golang.org/x/sys/windows/registry"
	"proxym/pkg/httputils"
	"sync"
)

type SetProxy uint32

const (
	SetProxyUnset SetProxy = iota
	SetProxyDisable
	SetProxyEnable
)

func setProxy(enable SetProxy, address string) (ret error) {
	key, err := registry.OpenKey(
		registry.CURRENT_USER,
		"Software\\Microsoft\\Windows\\CurrentVersion\\Internet Settings",
		registry.QUERY_VALUE|registry.SET_VALUE,
	)
	if err != nil {
		return err
	}
	defer func() {
		retKey := key.Close()
		if ret == nil {
			ret = retKey
		}
	}()
	if enable != SetProxyUnset {
		err = key.SetDWordValue("ProxyEnable", uint32(enable)-1)
		if err != nil {
			return err
		}
	}
	if address != "" {
		err = key.SetStringValue("ProxyServer", address)
		if err != nil {
			return err
		}
	}
	return nil
}

func filterProxies(addresses chan string, ctx context.Context) chan string {
	sink := make(chan string, len(addresses))
	go func() {
		wg := new(sync.WaitGroup)
		for address := range addresses {
			wg.Add(1)
			go func(address string) {
				defer wg.Done()
				if _, err := httputils.GetProxied("https://google.com", address, ctx); err == nil {
					sink <- address
				}
			}(address)
		}
		wg.Wait()
		close(sink)
	}()
	return sink
}
