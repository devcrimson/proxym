package readers

import (
	"context"
	"fmt"
	"sync"
)

func Read(ctx context.Context) chan string {
	sink := make(chan string)
	wg := new(sync.WaitGroup)
	wg.Add(1)
	go func() {
		defer wg.Done()
		net, err := readFreeProxyListNet(ctx)
		if err != nil {
			fmt.Println("Cannot get from free-proxy-list.net:", err)
			return
		}
		for _, n := range net {
			sink <- n
		}
	}()
	go func() {
		wg.Wait()
		close(sink)
	}()
	return sink
}
