package readers

import (
	"context"
	"fmt"
	"github.com/PuerkitoBio/goquery"
	"proxym/pkg/httputils"
)

func readFreeProxyListNet(ctx context.Context) ([]string, error) {
	resp, err := httputils.Get("https://free-proxy-list.net/", ctx)
	if err != nil {
		return nil, err
	}
	doc, err := goquery.NewDocumentFromReader(resp.Body)
	if err != nil {
		panic(err)
	}
	return doc.
		Find("table.table.table-striped.table-bordered tbody tr").
		Map(func(i int, selection *goquery.Selection) string {
			td := selection.Find("td")
			proto := "http"
			if td.Eq(6).Text() == "yes" {
				proto += "s"
			}
			return fmt.Sprintf(
				"%s://%s:%s",
				proto,
				td.Eq(0).Text(),
				td.Eq(1).Text(),
			)
		}), nil
}
