package main

import "sync"

type AddressHolder struct {
	cursor int
	inner  []string
	mu     *sync.Mutex
}

func (p *AddressHolder) Consume(c chan string) {
	p.mu.Lock()
	p.inner = nil
	p.mu.Unlock()
	for proxy := range c {
		p.mu.Lock()
		p.inner = append(p.inner, proxy)
		p.mu.Unlock()
	}
}

func (p *AddressHolder) Current() string {
	p.mu.Lock()
	item := p.inner[p.cursor]
	p.mu.Unlock()
	return item
}

func (p *AddressHolder) Next() string {
	p.mu.Lock()
	item := p.inner[p.cursor]
	p.cursor = (p.cursor + 1) % len(p.inner)
	p.mu.Unlock()
	return item
}

func (p *AddressHolder) Len() int {
	return len(p.inner)
}

func (p *AddressHolder) IsEmpty() bool {
	return p.Len() == 0
}

func newAddressHolder() *AddressHolder {
	return &AddressHolder{
		cursor: 0,
		inner:  nil,
		mu:     new(sync.Mutex),
	}
}
