package main

import (
	"context"
	"fmt"
	"net/http"
	"os"
	"proxym/cmd/service/readers"
	"proxym/internal/sharedinfo"
	"strconv"
	"time"
)

func main() {
	//if !windows.GetCurrentProcessToken().IsElevated() {
	//	panic("proxym must be run as administrator")
	//}
	proxies := newAddressHolder()
	if err := http.ListenAndServe(
		":"+strconv.Itoa(sharedinfo.Port),
		http.HandlerFunc(func(writer http.ResponseWriter, request *http.Request) {
			handle(writer, request, proxies)
		}),
	); err != nil {
		panic(err)
	}
}

func handle(writer http.ResponseWriter, request *http.Request, proxies *AddressHolder) {
	action := sharedinfo.TryWrap(request.URL.Path[1:])
	switch action {
	case sharedinfo.ActionRefresh:
		ctx, _ := context.WithTimeout(context.Background(), time.Second*15)
		proxies.Consume(filterProxies(readers.Read(ctx), ctx))
		fmt.Println("Consumed", proxies.Len(), "proxies")
		_, _ = writer.Write([]byte("Success"))
	case sharedinfo.ActionRotate:
		if proxies.IsEmpty() {
			writer.WriteHeader(500)
			_, _ = writer.Write([]byte("Proxies list is empty. Call /refresh and wait"))
		} else if setProxy(SetProxyUnset, proxies.Next()) != nil {
			writer.WriteHeader(500)
			_, _ = writer.Write([]byte("Cannot set proxy"))
		} else {
			_, _ = writer.Write([]byte("Success"))
		}
	case sharedinfo.ActionGet:
		if proxies.IsEmpty() {
			writer.WriteHeader(500)
			_, _ = writer.Write([]byte("Proxies list is empty. Call /refresh and wait"))
		} else {
			_, _ = writer.Write([]byte(proxies.Current()))
		}
	case sharedinfo.ActionToggleOn:
		if setProxy(SetProxyEnable, "") != nil {
			writer.WriteHeader(500)
			_, _ = writer.Write([]byte("Cannot set proxy"))
		} else {
			_, _ = writer.Write([]byte("Success"))
		}
	case sharedinfo.ActionToggleOff:
		if setProxy(SetProxyDisable, "") != nil {
			writer.WriteHeader(500)
			_, _ = writer.Write([]byte("Cannot set proxy"))
		} else {
			_, _ = writer.Write([]byte("Success"))
		}
	case sharedinfo.ActionShutdown:
		_, _ = writer.Write([]byte("System will be shutdown in 3 seconds"))
		defer func() {
			time.Sleep(time.Second * 3)
			os.Exit(0)
		}()
	}
}
