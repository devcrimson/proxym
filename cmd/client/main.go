package main

import (
	"flag"
	"fmt"
	"io"
	"net/http"
	"proxym/internal/sharedinfo"
	"strings"
)

var RawAction string

func init() {
	flag.StringVar(&RawAction, "action", "", "")
	flag.Parse()
}

func main() {
	action := sharedinfo.MapToAction(RawAction)
	if action == sharedinfo.ActionInvalid {
		panic(fmt.Sprintf(
			"Invalid action, expected '%s'",
			strings.Join(sharedinfo.Actions(), "' '"),
		))
	}
	sendRequest(action)
}

func sendRequest(action sharedinfo.Action) {
	get, err := http.Get(fmt.Sprintf("http://localhost:%d/%d", sharedinfo.Port, action))
	if err != nil {
		panic(err)
	}
	all, err := io.ReadAll(get.Body)
	if err != nil {
		panic(err)
	}
	fmt.Println(string(all))
}
