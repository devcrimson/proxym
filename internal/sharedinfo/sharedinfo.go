package sharedinfo

import "strconv"

const Port = 9678

type Action int

const (
	ActionInvalid Action = iota - 1
	ActionRefresh
	ActionRotate
	ActionGet
	ActionToggleOn
	ActionToggleOff
	ActionShutdown
)

func TryWrap(value string) Action {
	if len(value) != 1 {
		return ActionInvalid
	}
	i, err := strconv.Atoi(value)
	if err != nil || i < 0 || i > 5 {
		return ActionInvalid
	}
	return Action(i)
}

func MapToAction(value string) Action {
	switch value {
	case "refresh":
		return ActionRefresh
	case "rotate":
		return ActionRotate
	case "get":
		return ActionGet
	case "toggle-on":
		return ActionToggleOn
	case "toggle-off":
		return ActionToggleOff
	case "shutdown":
		return ActionShutdown
	}
	return ActionInvalid
}

func MapToString(action Action) string {
	switch action {
	case ActionRefresh:
		return "refresh"
	case ActionRotate:
		return "rotate"
	case ActionGet:
		return "get"
	case ActionToggleOn:
		return "toggle-on"
	case ActionToggleOff:
		return "toggle-off"
	case ActionShutdown:
		return "shutdown"
	}
	return "invalid"
}

func Actions() []string {
	return []string{
		"refresh", "rotate", "get",
		"toggle-on", "toggle-off", "shutdown",
	}
}
